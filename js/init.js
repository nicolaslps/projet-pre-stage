$(document).ready(function() {
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
        dataJSON = JSON.parse(this.responseText);
        setTimeout(function(){ 
            console.log("JSON charged")
            document.getElementById("data-content").addEventListener("load", loadItem('data'));
            document.getElementById("visualize-content").addEventListener("load", loadItem('visualize'));
            document.getElementById("model-content").addEventListener("load", loadItem('model'));
            document.getElementById("evaluate-content").addEventListener("load", loadItem('evaluate'));
            document.getElementById("unsupervised-content").addEventListener("load", loadItem('unsupervised'));
        }, 100)
        
    }
    };
    xmlhttp.open("GET", "./json/items.json", true);
    xmlhttp.send(); 
});

$(window).on('load',function() {
    $('#loader').hide(1000);
});

function loadItem(type) {   
    targetObjet = type+"-content";
    console.log(dataJSON)

    let numberOfItem = 0;

    dataJSON.listeItem.forEach(element => {
        if (element.type == type) {

            if (numberOfItem == 0 ) {
                let divId = create_UUID();
                document.getElementById(targetObjet).innerHTML += "<div class='row my-4' id="+divId+">"
                actuelDiv = document.getElementById(divId);
            }
            
            id = create_UUID();
            let dataonmouseOver = "onmouseover='loadDescription('"+element.key+"')";
            //document.getElementById(targetObjet).innerHTML = document.getElementById(targetObjet).innerHTML + "<div class='mx-2'    onmouseover='loadDescription(\""+element.key+"\")'   type-action='"+element.key+"'   id='"+id+"' draggable='true' style='background-image:url(./img/"+element.image_file_name+"); width: 75px; height: 75px; background-size: 100%;'  ondragstart='dragstart_handler(event);' ><div class='listedItemTitle'>"+element.name+"</div></div>";
            actuelDiv.innerHTML = actuelDiv.innerHTML + "<div class='mx-2'    onmouseover='loadDescription(\""+element.key+"\")'   type-action='"+element.key+"'   id='"+id+"' draggable='true' style='background-image:url(./img/"+element.image_file_name+"); width: 75px; height: 75px; background-size: 100%;'  ondragstart='dragstart_handler(event);' ><div class='listedItemTitle' data-toggle='tooltip' data-placement='bottom' title='"+element.name+"'>"+element.name+"</div></div>";
            numberOfItem++;
            if(numberOfItem > 4){
                numberOfItem = 0;
            }
        }

    });
}
 