function newItem(x,y){
    let id = create_UUID();    
    getValueOf(currentItemType);
    createElement(id, x, y);
    makeNewItemDraggable(id)  
}
 

function createElement(id, x, y){
    
    var newItem = document.createElement('DIV');
    
    newItem.id = id;
    newItem.classList.add("draggableItem");
    newItem.setAttribute('style','will-change: transform; transform: translate(0px);'); 
    newItem.style.position = "absolute";
    x = x-50;
    newItem.style.left = x+"px";   
    y = y-65
    newItem.style.top = y+"px";   
    newItem.setAttribute('data-type', currentKey);



    var draggable = document.createElement('DIV');
    draggable.classList.add("draggable"); 
    draggable.setAttribute('style', 'cursor: grab; user-select: none;');
    draggable.style.backgroundImage  = "url(./img/"+currentImage; 
    draggable.ondblclick = function(){
        $("#settingsPanel").removeAttr("hidden");
        $("#settingsPanel").animate({width: "20vmax"});
        $("#closeSettingsPanel").removeAttr("hidden");
        
        removeActiveAttrFromItems()
        $("#"+id+">div").addClass("active"); 
        let selectedType = $("#"+id).attr("data-type")
        loadSetting(selectedType);

        
    }
     


    var itemTitle = document.createElement('DIV');
    itemTitle.classList.add("item-title");
    itemTitle.innerHTML =  currentName;

    if (currentInput) { 
        var input = document.createElement('DIV');
        input.classList.add("input-square")
        input.draggable = true;
        input.ondrop = function () {
            actualInpPut = id;
            createALink()
            setTimeout(function(){ droppable = true; }, 1000);
            
        }
        input.ondragstart = function (ev) {
            droppable = false;
            actualInpPut = id;
            getValueOf($("#"+id).attr("data-type")); 
        }
        input.ondragend = function (){
            setTimeout(function(){ droppable = true; }, 1000);
        }
    }

    if (currentOutput) { 
        var output = document.createElement('DIV');
        output.classList.add("output-square")
        output.draggable = true;
        output.ondragstart = function () {
            droppable = false;
            actualOutPut = id;
            getValueOf($("#"+id).attr("data-type")); 
            
        }
        output.ondragend = function (){
            setTimeout(function(){ droppable = true; }, 600);
        }
        output.ondrop = function () {
            actualOutPut = id;
            createALink()
            setTimeout(function(){ droppable = true; }, 500);
            
        }
    }



    newItem.onmousedown = fixAllLine;
    draggable.onmousedown = fixAllLine;

    draggable.appendChild(itemTitle);
    newItem.appendChild(draggable);

    if (currentInput) { 
        newItem.appendChild(input);
    }

    if (currentOutput) { 
        newItem.appendChild(output);
    }
    
    


    newItem.addEventListener("pointermove", fixAllLine)

    var dragZone = document.getElementById('dest_copy');
    dragZone.appendChild(newItem);
   
}

function makeNewItemDraggable(id){
    'use strict';
    new PlainDraggable(document.getElementById(id),{
        handle: document.querySelector( "div#"+ id + ' .draggable'),
        autoScroll: document.getElementById("main-drop-zone")
    });    


}

function getValueOf(key) {   
    dataJSON.listeItem.forEach(element => {
        if (element.key == key) {
            currentKey = element.key; 
            currentName = element.name;
            currentType = element.type;
            currentImage = element.image_file_name ;
            currentInput = element.input ;
            currentOutput = element.output ;
            currentOutputDataType = element.output_data_type;
        }
        
    })

}





function disableInput(input){
    inputToDisable = document.querySelector( "div#"+ input + ' .input-square') ;
    inputToDisable.style.pointerEvents = "none";
    disabeledInput = input;
}

function enableInput(input){
    inputToDisable = document.querySelector( "div#"+ input + ' .input-square') ;
    inputToDisable.style.pointerEvents = "";
}

function removeActiveAttrFromItems(){
    $("#dest_copy>div>").removeClass("active");
}