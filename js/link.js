function createALink(){
    let linkable = false;
    let outputType = document.getElementById(actualOutPut).getAttribute("data-type");
    let inputType = document.getElementById(actualInpPut).getAttribute("data-type");
  
    dataJSON.listeItem.forEach(element => {
        if (element.key == outputType) {
            linkable = element.linkableTo.includes(inputType);           
        }
    })



    if (linkable) {
        var id = new LeaderLine(LeaderLine.pointAnchor( document.querySelector( "div#"+ actualOutPut + ' .output-square'), {x: 20 }), LeaderLine.pointAnchor( document.querySelector( "div#"+ actualInpPut + ' .input-square'), {x: 10 }) ,{startSocket: 'right', endSocket: 'left',color: '#a3a3a3', startPlug: 'square', endPlug: 'arrow1', startPlugColor: 'rgb(78, 78, 77)', endPlugColor: 'rgb(78, 78, 77)' });
        

        id.middleLabel = LeaderLine.captionLabel({text: currentOutputDataType, color: '#000'});
        listeLine.push(id);
        disableInput(actualInpPut);
        fixAllLine();
    }else{
        $(document).ready(function(){
            $("#toastError").toast('show');
        });
    }

}





function fixAllLine() {
    listeLine.forEach(element => {
        element.position();
    });
}