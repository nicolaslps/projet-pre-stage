function loadSetting(selectedType){
    console.log("loadSettings launch")
    dataJSON.listeItem.forEach(element => { 
        if (element.key == selectedType) {
            document.getElementById("dynamicSettings").innerHTML = "";
       
            element.service.config.forEach(parameter => {
                

                switch (parameter.type) {
                    case "file":
                        document.getElementById("dynamicSettings").innerHTML += "<div class='form-group'>" 
                        document.getElementById("dynamicSettings").innerHTML += "<label>"+parameter.label+"</label>" 
                        document.getElementById("dynamicSettings").innerHTML += "<input type='file' class='form-control-file'><br>" 
                        document.getElementById("dynamicSettings").innerHTML += "</div>" 
                        break;

                    case "url":
                        document.getElementById("dynamicSettings").innerHTML += "<div class='form-group'>" 
                        document.getElementById("dynamicSettings").innerHTML += "<label>"+parameter.label+"</label>" 
                        document.getElementById("dynamicSettings").innerHTML += "<input type='url' class='form-control'><br>" 
                        document.getElementById("dynamicSettings").innerHTML += "</div>" 
                        break;

                    case "text":
                        document.getElementById("dynamicSettings").innerHTML += "<div class='form-group'>" 
                        document.getElementById("dynamicSettings").innerHTML += "<label>"+parameter.label+"</label>" 
                        document.getElementById("dynamicSettings").innerHTML += "<input type='text' class='form-control'><br>" 
                        document.getElementById("dynamicSettings").innerHTML += "</div>" 
                        break;

                    case "list":
                        document.getElementById("dynamicSettings").innerHTML += "<div class='form-group'>" 
                        document.getElementById("dynamicSettings").innerHTML += "<label>"+parameter.label+"</label>" 
                        listId=create_UUID();
                        let list = "<select class='form-control' id='"+listId+"'>"
                        
                        parameter.listOption.forEach( option => { 
                            list += "<option value='"+option+"'>"+option+"</option>"
                            }
                        ) 
                        list += "</select><br>"
                        document.getElementById("dynamicSettings").innerHTML += list 
                        document.getElementById("dynamicSettings").innerHTML += "</div>" 
                        break;

                    case "number":
                        document.getElementById("dynamicSettings").innerHTML += "<div class='form-group'>" 
                        document.getElementById("dynamicSettings").innerHTML += "<label>"+parameter.label+"</label>" 
                        document.getElementById("dynamicSettings").innerHTML += "<input type='number' class='form-control'><br>" 
                        document.getElementById("dynamicSettings").innerHTML += "</div>" 
                        break;


                    default:
                        break;
                }

                
            });

            
        }

    });
}

$(document).ready(function() {
    $("#closeSettingsPanel").click(function(){            
        $("#settingsPanel").animate({width: "0px"});
        $("#settingsPanel").attr("hidden", "hidden")
        $("#closeSettingsPanel").attr("hidden", "hidden")
        removeActiveAttrFromItems()
    });
});
 